import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  allUser: User[] =[];
  constructor(private userService : UserService,private router : Router) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe((data:any) =>{
      this.allUser = data;
    })
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['']);
  }

  deleteItem(id:number){
     this.userService.delete(id).subscribe({
      next: (data:any) => {
          this.allUser = this.allUser.filter(_ => _.id != id)
      }
     })
  }

}
