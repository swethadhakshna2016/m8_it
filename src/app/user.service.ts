import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable,of, throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient : HttpClient,private router : Router) { }

 getAll(){
  return this.httpClient.get<User[]>('http://localhost:3000/user')
 }

 create(data:User): Observable<any>{
  return this.httpClient.post('http://localhost:3000/user',data)
 }


 edit(id:number){
  return this.httpClient.get<User>(`http://localhost:3000/user/${id}`)
 }

 update(data:User){
  return this.httpClient.put<User[]>(`http://localhost:3000/user/${data.id}`,data)
 }

 delete(id:number){
  return this.httpClient.delete<User>(`http://localhost:3000/user/${id}`)
 }

 login({ username, password }: any): Observable<any> {
  console.log(123);
  console.log(username);
  if (username === 'Superadmin' && password === 'admin@123') {
    localStorage.setItem('token', 'Superadmin');
    return of({ adminname:'Superadmin'});
  }
  else{
     return throwError(new Error('Failed to login'));
  }
 
} 


isLoggedIn() {
  return localStorage.getItem('token') !== null;
}

}
