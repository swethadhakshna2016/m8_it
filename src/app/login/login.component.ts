import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm!: FormGroup;
  constructor(private router : Router, private userService : UserService ) { }

  ngOnInit(): void {
    this.createForm(); 
  }

  createForm(){
    this.loginForm = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    })
  }

  login(){
    if (!this.loginForm.valid) {
      this.loginForm.markAllAsTouched(); 
      return;    
    }

    if (this.loginForm.valid){
      this.userService.login(this.loginForm.value).subscribe(
        (result) => {
          console.log(result);
          this.router.navigate(['/dashboard']);
          
        },
      );
    }
  }

}
