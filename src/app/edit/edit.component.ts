import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  constructor( private userService : UserService, private router : Router, private route : ActivatedRoute) { }

  formdata : User  = {
    id:0,
    name:'',
    city:'',
    email:''
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param) =>{
      let id = Number(param.get('id'))
      this.getById(id)
    })
  }

  update(){
    this.userService.update(this.formdata).subscribe({
      next:(data:any)=>{
        this.router.navigate(['/home']);
      }
    })
  }

  getById(id:number){
   this.userService.edit(id).subscribe((data) =>{
      this.formdata = data;
   })
  }

}
