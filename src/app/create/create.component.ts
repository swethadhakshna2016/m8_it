import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  constructor(private userService: UserService, private router : Router) { }

   formdata : User = {
    id:0,
    name:'',
    city:'',
    email:''
   }
  ngOnInit(): void {
  }

  create(){
    this.userService.create(this.formdata).subscribe({
      next:(data:any)=>{
        this.router.navigate(['/home']);
      }
    })
  }

}
