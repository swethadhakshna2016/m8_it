import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  forgotForm!: FormGroup
  constructor(private router : Router) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm(){
    this.forgotForm = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    })
  }


  close(){
    this.router.navigate(['/dashboard']);
  }

}
